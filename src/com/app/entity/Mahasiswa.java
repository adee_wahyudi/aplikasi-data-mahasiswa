/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ADE
 */
public class Mahasiswa {
    private String npm;
    private String nama;
    private String jurusan;
    private String alamat;
    private String telp;

    public Mahasiswa() {
    }

    public Mahasiswa(String npm, String nama, String jurusan, String alamat, String telp) {
        this.npm = npm;
        this.nama = nama;
        this.jurusan = jurusan;
        this.alamat = alamat;
        this.telp = telp;
    }
    
    public Mahasiswa(ResultSet rs) throws SQLException {
        this.npm = rs.getString("npm");
        this.nama = rs.getString("nama");
        this.jurusan = rs.getString("jurusan");
        this.alamat = rs.getString("alamat");
        this.telp = rs.getString("telp");
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }
    
    public Object getValue(int index){
        switch (index){
            case 0:
                return npm;
            case 1:
                return nama;
            case 2:
                return jurusan;
            case 3:
                return alamat;
            case 4:
                return telp;
            default:
                return "";
        }
    }
}
