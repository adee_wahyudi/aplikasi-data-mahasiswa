/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.gui.util;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author ADE
 */
public class ConnectionUtil {
    private static Connection conn;

    public static Connection getConnection() throws SQLException {
        if(conn == null){
            MysqlDataSource data = new MysqlDataSource();
            data.setServerName("localhost");
            data.setDatabaseName("stmik_banisaleh");
            data.setUser("root");
            data.setPassword("qwerty");
            conn = data.getConnection();
        }
        return conn;
    }
    
}

